@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">

                        <h1>General Posting Guidelines</h1>
                        <p>Allow other users to listen to you easily by following our guidelines</p>

                        <h3>1. Try using the search engine first</h3>
                        <p>By using the search, the site will retrieve all solutions available in the forum and even in tutorials. The search is available on all pages, just on top.</p>

                        <h3>2. Be DESCRIPTIVE and CONCISE</h3>
                        <p>Post a descriptive topic name. Give a short summary of your problem IN THE SUBJECT.  (Don’t use attention getting subjects. They don’t get attention and only annoy people).</p>

                        <h3>3. Be patient</h3>
                        <p>Be patient, help people out by posting good descriptions of what you need help with. Do not insult others with aggressive terms.</p>

                        <h1>Forum Rules</h1>

                        <h3>1. Always post in the right forum/topic</h3>
                        <p>Do not post a question in a forum that is not related to the problem you have.</p>

                        <h3>2. Make sure someone else has not already asked the same question</h3>
                        <p>Search in the forum to verify if someone has already asked the same question.</p>

                        <h3>3. Don't post the same topic/answer many times</h3>
                        <p>Be patient. Do not repeat the same question trying to attract users' attention.</p>

                        <h3>4. Use full words not their short form</h3>
                        <p>Here is not SMS chat. Please write words in their full form. Otherwise others will have difficulties getting you.</p>

                        <h3>5. Be patient with beginners</h3>
                        <p>Do not offense those who are new or are beginners. Help them find their way.</p>

                        <h3>6. Do not post copyright-infringing material</h3>
                        <p>Providing or asking for information on how to illegally obtain copyrighted materials is forbidden.</p>

                        <h3>7. No Spam or Advertising</h3>
                        <p>These forums define spam as unsolicited advertisement for goods, services and/or other web sites, or posts with little, or completely unrelated content. Do not spam the forums with links to your site or product, or try to self-promote your website, business or forums etc.</p>

                        <h3>8. Remain respectful of other members</h3>
                        <p>All posts should be professional and courteous. You have every right to disagree with your fellow community members and explain your perspective. But that doesn't mean you can attack or insult others. Use the "upvote" and "downvote" but do not degrade the user who posted it.</p>

                        <h3>9. Do not be "offensive"</h3>
                        <p>Any material which constitutes defamation, harassment, or abuse is strictly prohibited. Material that is sexually or otherwise obscene, racist, or otherwise overly discriminatory is not permitted on these forums. This includes user pictures.</p>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
//use Illuminate\Foundation\Testing\DatabaseMigrations;

class ParticipateInForumTest extends TestCase
{
    use RefreshDatabase;
//    protected $guarded=[];
//    public function setup(){
//        parent::setup();
//        $this->thread = factory('App\Thread')->create();
//    }
      public function testuserParticipate(){
          $this->be($user =factory('App\User')->create());
          $thread = factory('App\Thread')->create();
          $reply = factory('App\Reply')->create();
          $this->post('/threads'.$thread->id.'/replies',$reply->toArray());
          $this->get( $thread->path())->assertSee($reply->body);
      }
}
